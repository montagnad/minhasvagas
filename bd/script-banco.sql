CREATE TABLE empresa (
                id INT AUTO_INCREMENT NOT NULL,
                nomeEmpresa VARCHAR(500) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE setor (
                id INT AUTO_INCREMENT NOT NULL,
                nomeSetor VARCHAR(100) NOT NULL,
                id_empresa INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE vagas (
                id INT AUTO_INCREMENT NOT NULL,
                nomeVaga VARCHAR(100) NOT NULL,
                requisitos VARCHAR(500) NOT NULL,
                id_setor INT NOT NULL,
                PRIMARY KEY (id)
);

CREATE TABLE usuarios (
  id int(11) NOT NULL,
  nomeUsuario varchar(100) NOT NULL,
  permissao varchar(20) NOT NULL,
  senha varchar(32) NOT NULL
);


ALTER TABLE setor ADD CONSTRAINT empresa_setor_fk
FOREIGN KEY (id_empresa)
REFERENCES empresa (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE vagas ADD CONSTRAINT setor_vaga_fk
FOREIGN KEY (id_setor)
REFERENCES setor (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

INSERT INTO usuarios(nomeUsuario,permissao,senha) VALUES ('teste','administrador','698dc19d489c4e4db73e28a713eab07b');
