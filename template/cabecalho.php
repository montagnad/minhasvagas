<?php
    session_start(); //DEVE SER A PRIMEIRA LINHA

    if (!isset($_SESSION['logado'])) {
      header('Location: http://localhost/minhasvagas/login.php');
    }
    //Finaliza a sessão logado da Aplicação
    if(isset($_GET['acao']) && $_GET['acao']=="sair"){
        unset($_SESSION['logado']);
    }
?>

<?php define('BASE_URL', 'http://localhost/minhasvagas'); ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.png">

    <title>Vagas de emprego | Sulmaq part of Marel</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/sticky-footer-navbar/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="sticky-footer-navbar.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
        <img src="<?= BASE_URL; ?>/template/logo2.png" width="80">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?= BASE_URL; ?>/vaga/vaga.php" style="color: #394E70;">Vagas<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= BASE_URL; ?>/setor/setor.php" style="color: #394E70;">Setores</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= BASE_URL; ?>/empresa/empresa.php" style="color: #394E70;">Empresas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= BASE_URL; ?>/usuario/usuarios.php" style="color: #394E70;">Usuários</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= BASE_URL; ?>/login.php?acao=sair" style="color: #394E70;">Sair</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Begin page content -->
    <main>
    <br><br><br>
