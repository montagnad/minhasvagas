<?php
    require_once '../config/conexao.php';

    //epsodio/epsodio.php?acao=listar

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT v.id, v.nomeVaga, v.requisitos, s.nomeSetor as setor, e.nomeEmpresa as empresa
                    FROM vagas v
                    INNER JOIN setor s ON s.id=v.id_setor
                    INNER JOIN empresa e ON e.id=s.id_empresa
                    ORDER BY empresa";
       $query = $con->query($sql);
       if($query==false) print_r($con->errorInfo());
       $registros = $query->fetchAll();

       // print_r($registros); exit;
       require_once '../template/cabecalho.php';
       require_once 'lista_vaga.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      $lista_setor = getSetores();

      // print_r($lista_genero); exit;
      require_once '../template/cabecalho.php';
      require_once 'form_vaga.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        $sql = "INSERT INTO vagas(nomeVaga, requisitos, id_setor)
                  VALUES(:nomeVaga, :requisitos, :id_setor)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./vaga.php');
        }else{
            print_r($registro);
            echo "Erro ao tentar inserir o registro, msg: " . print_r($query->errorInfo());
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM vagas WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./vaga.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM vagas WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;
        $lista_setor = getSetores();
        require_once '../template/cabecalho.php';
        require_once 'form_vaga.php';
        require_once '../template/rodape.php';
    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE vagas SET nomeVaga = :nomeVaga, requisitos = :requisitos,
                    id_setor = :id_setor WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nomeVaga', $_POST['nomeVaga']);
        $query->bindParam(':requisitos', $_POST['requisitos']);
        $query->bindParam(':id_setor', $_POST['id_setor']);
        $result = $query->execute();

        if($result){
            header('Location: ./vaga.php');
        }else{
            echo "Erro ao tentar atualizar os dados" . print_r($query->errorInfo());
        }
    }

    //arrumar isso
    function getSetores(){
        $sql   = "SELECT s.id, s.nomeSetor, s.id_empresa, e.nomeEmpresa as nomeEmpresa
        FROM setor s
        INNER JOIN empresa e ON e.id=s.id_empresa
        ORDER BY e.nomeEmpresa";
        $query = $GLOBALS['con']->query($sql);
        $lista_setor = $query->fetchAll();
        return $lista_setor;
    }
 ?>
