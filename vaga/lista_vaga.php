
<div class="container print">
  <h2>Vagas de emprego</h2>
  <a class="btn btn-info" href="vaga.php?acao=novo">Nova vaga de emprego</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Vaga</th>
          <th>Requisitos</th>
          <th>Setor</th>
          <th>Empresa</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?= $linha['id']; ?></td>
            <td><?= $linha['nomeVaga']; ?></td>
            <td><?= $linha['requisitos']; ?></td>
            <td><?= $linha['setor']; ?></td>
            <td><?= $linha['empresa']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="vaga.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="vaga.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
