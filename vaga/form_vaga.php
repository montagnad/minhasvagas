<?php
    if(isset($registro)) $acao = "vaga.php?acao=atualizar&id=".$registro['id'];
    else $acao = "vaga.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nomeVaga">Nome da vaga</label>
      <input id="nomeVaga" class="form-control" type="text" name="nomeVaga"
        value="<?php if(isset($registro)) echo $registro['nomeVaga']; ?>" required>
    </div>
    <div class="from-group">
      <label for="requisitos">Requisitos</label>
      <input id="requisitos" class="form-control" type="text" name="requisitos"
        value="<?php if(isset($registro)) echo $registro['requisitos']; ?>" maxlength="500" required>
    </div>
    <div class="from-group">
      <label for="id_setor">Setor</label>
      <select class="form-control" name="id_setor" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_setor as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_setor']==$item['id']) echo "selected";?>>
            <?php echo $item['nomeEmpresa'] . " | " . $item['nomeSetor'];?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Salvar</button>
  </form>
</div>
