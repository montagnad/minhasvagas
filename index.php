<?php
session_start();
    if (!isset($_SESSION['logado'])) {
      header('Location: login.php');
    }
 ?>

<?php require_once 'template/cabecalho.php'; ?>


<section class="jumbotron text-center">
  <div class="container">
    <h1 class="jumbotron-heading">Vagas de emprego</h1>
    <p class="lead text-muted">
      Selecione no menu superior a página desejada.
    </p>
  </div>
</section>

<?php require_once 'template/rodape.php'; ?>
