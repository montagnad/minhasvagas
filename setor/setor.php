<?php
    require_once '../config/conexao.php';

    //serie/serie.php?acao=listar

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT s.id, s.nomeSetor, e.nomeEmpresa as empresa
                    FROM setor s
                    INNER JOIN empresa e ON e.id=s.id_empresa";
       $query = $con->query($sql);
       $registros = $query->fetchAll();

       // print_r($registros); exit;
       require_once '../template/cabecalho.php';
       require_once 'lista_setor.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      $lista_empresa = getEmpresas();

      // print_r($lista_genero); exit;
      require_once '../template/cabecalho.php';
      require_once 'form_setor.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;



        $sql = "INSERT INTO setor (nomeSetor, id_empresa)
                  VALUES(:nomeSetor, :id_empresa);";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./setor.php');
        }else{
            echo "Erro ao tentar inserir o registro, msg: " . print_r($query->errorInfo());
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM setor WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./setor.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM setor WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;
        $lista_empresa = getEmpresas();
        require_once '../template/cabecalho.php';
        require_once 'form_setor.php';
        require_once '../template/rodape.php';
    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE setor SET nomeSetor = :nomeSetor,
                    id_empresa = :id_empresa WHERE id = :id";
        $query = $con->prepare($sql);



        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nomeSetor', $_POST['nomeSetor']);
        $query->bindParam(':id_empresa', $_POST['id_empresa']);
        $result = $query->execute();

        if($result){
            header('Location: ./setor.php');
        }else{
            echo "Erro ao tentar atualizar os dados" . print_r($query->errorInfo());
        }
    }

    //função que retorna a lista de gêneros cadastrados no banco
    function getEmpresas(){
        $sql   = "SELECT * FROM empresa";
        $query = $GLOBALS['con']->query($sql);
        $lista_empresa = $query->fetchAll();
        return $lista_empresa;
    }
 ?>
