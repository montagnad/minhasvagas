<?php
    if(isset($registro)) $acao = "setor.php?acao=atualizar&id=".$registro['id'];
    else $acao = "setor.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nome">Nome do setor</label>
      <input id="nomeSetor" class="form-control" type="text" name="nomeSetor"
        value="<?php if(isset($registro)) echo $registro['nomeSetor']; ?>" required>
    </div>
    <div class="from-group">
      <label for="id_empresa">Empresa</label>
      <select class="form-control" name="id_empresa" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_empresa as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_empresa']==$item['id']) echo "selected";?>>
            <?php echo $item['nomeEmpresa']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Salvar</button>
  </form>
</div>
