

<?php
    if(isset($registro)) $acao = "empresa.php?acao=atualizar&id=".$registro['id'];
    else $acao = "empresa.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nomeEmpresa">Razão social da empresa</label>
      <input id="nomeEmpresa" class="form-control" type="text" name="nomeEmpresa"
        value="<?php if(isset($registro)) echo $registro['nomeEmpresa']; ?>" required>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Salvar</button>
  </form>
</div>
